from django.shortcuts import render

def home(request):
    context = {}
    return render(request, 'templates/home.html',context)