from django.db import models

class Medical_list(models.Model):
    medical_name = models.CharField(max_length = 100)

    def __str__(self):
        return "{}".format(self.medical_name)

class Category(models.Model):
    category = models.ForeignKey(Medical_list, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length = 100)

    def __str__(self):
        return "{}, {}".format(self.category, self.name)

class Doctor(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length = 50)
    surname = models.CharField(max_length = 50)
    age = models.IntegerField(default=None)
    carrier = models.CharField(max_length = 1000)

    def __str__(self):
        return "{}, {}, {}, {}, {} ".format(self.name, self.surname, self.age, self.carrier)

class Reserve(models.Model):
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    name = models.CharField(max_length = 50)
    surname = models.CharField(max_length = 50)
    age = models.IntegerField(default=None)
    rezerve_time = models.DateTimeField()
    contact_number = models.CharField(max_length = 15)
    comment = models.CharField(max_length = 1000)

    def __str__(self):
        return "{}, {}, {}, {}, {}".format(self.name, self.surname, self.age, self.rezerve_time, self.comment)
    
