from django.contrib import admin
from .models import Medical_list, Category, Doctor, Reserve

@admin.register(Medical_list)
class Medical_listAdmin(admin.ModelAdmin):
    list_display = ('id', 'medical_name')

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

@admin.register(Doctor)
class DoctorAdmin(admin.ModelAdmin):
    list_display = ('name', 'surname', 'age', 'carrier')

@admin.register(Reserve)
class ReserveAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'surname', 'age', 'rezerve_time', 'comment', 'contact_number')

